//
//  Colors.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 13.01.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#ifndef Colors_h
#define Colors_h

#define greenColor       [UIColor colorWithRed:0.0/255.0 green:153.0/255.0 blue:76.0/255.0 alpha:1.0]
#define yellowColor      [UIColor colorWithRed:250.0/255.0 green:235.0/255.0 blue:28.0/255.0 alpha:1.0]
#define orangeColor      [UIColor colorWithRed:250.0/255.0 green:161.0/255.0 blue:28.0/255.0 alpha:1.0]
#define lightRedColor    [UIColor colorWithRed:255.0/255.0 green:85.0/255.0 blue:0.0/255.0 alpha:1.0]
#define darkRedColor     [UIColor colorWithRed:199.0/255.0 green:36.0/255.0 blue:30.0/255.0 alpha:1.0]
#define vinousColor      [UIColor colorWithRed:209.0/255.0 green:31.0/255.0 blue:97.0/255.0 alpha:1.0]
#define lightPurpleColor [UIColor colorWithRed:168.0/255.0 green:41.0/255.0 blue:126.0/255.0 alpha:1.0]
#define darkPurpleColor  [UIColor colorWithRed:118.0/255.0 green:16.0/255.0 blue:115.0/255.0 alpha:1.0]

#endif /* Colors_h */
