//
//  ParserRequest.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 09.01.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParserRequest : NSObject

+(ParserRequest *)sharedInstance;
-(NSMutableArray *)parseAnswer:(NSString *)answer;

@end
