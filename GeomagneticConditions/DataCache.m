//
//  DataCache.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 09.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import "DataCache.h"
#import "SolarActivityItem.h"
#import <MagicalRecord/MagicalRecord.h>
#import "SolarEntity+CoreDataClass.h"
#import "SolarEntity+CoreDataProperties.h"

@implementation DataCache


static DataCache *_sharedInstance = nil;
+(DataCache *)sharedInstance{
    @synchronized(self) {
        if (!_sharedInstance) {
            _sharedInstance = [[DataCache alloc] init];
        }
    }
    return _sharedInstance;
}


-(void)saveDataInCache:(NSArray *)solarItems {
    for(id array in solarItems){
        for (id item in array){
            [self saveSolarItem: item];
        }
    }
    [self notifyDataListener];
}


-(void)setListener:(void (^)(void)) listener {
    self.dataChangeListener = listener;
}


-(void) notifyDataListener { // уведомить слушателя
    if(self.dataChangeListener) {
        self.dataChangeListener();
    }
}


-(void)saveSolarItem:(SolarActivityItem *)newItem {
    SolarEntity * item = [self findSolarItem:newItem];
    if(item){
        [self deleteSolarItem:item];
    }
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        SolarEntity *newSolarEntity = [SolarEntity MR_createEntityInContext:localContext];
        newSolarEntity.kIndex = newItem.kIndex;
        newSolarEntity.dayMonth= newItem.dayMonthPeriod;
        newSolarEntity.startTime= newItem.timeStartPeriod;
        newSolarEntity.middleTime= newItem.timeMiddlePeriod;
        newSolarEntity.endTime= newItem.timeEndPeriod;
        newSolarEntity.dateTime = newItem.dateTimePeriod;
        newSolarEntity.unixTime = [NSString stringWithFormat:@"%f", newItem.unixTimeStartPeriod];
    }];
}


-(SolarEntity *)findSolarItem:(SolarActivityItem *)newItem {
    NSArray* itemsEntityWithAttribute = [SolarEntity MR_findByAttribute:@"dateTime" withValue:newItem.dateTimePeriod];
    if(itemsEntityWithAttribute) {
        for (id item in itemsEntityWithAttribute) {
            SolarEntity* oldItem = item;
            double oldTime = [oldItem.unixTime doubleValue];
            double newTime = newItem.unixTimeStartPeriod;
            if(newTime - oldTime < 3*24*60*60) {
                
            return oldItem;
            }
        }
    }
    return nil;
}

-(NSArray *)findAll{
    return [SolarEntity MR_findAllSortedBy:@"unixTime"
                                 ascending:NO];
}


-(NSArray *)getSolarItems{
    NSArray *entityItems = [self findAll];
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    for(id entity in entityItems) {
        SolarActivityItem * soralItem = [[SolarActivityItem alloc] init];
        SolarEntity * entityItem = entity;
        soralItem.kIndex = entityItem.kIndex;
        soralItem.timeStartPeriod = entityItem.startTime;
        soralItem.timeMiddlePeriod = entityItem.middleTime;
        soralItem.timeEndPeriod = entityItem.endTime;
        soralItem.dayMonthPeriod = entityItem.dayMonth;
        soralItem.dateTimePeriod = entityItem.dateTime;
        soralItem.unixTimeStartPeriod = [entityItem.unixTime doubleValue];
        [tmpArray addObject:soralItem];
    }
    NSArray * solarItems = [[NSArray alloc] initWithArray:tmpArray];
    return solarItems;
}


-(NSArray *)findAllForForecast{
    return [SolarEntity MR_findAllSortedBy:@"unixTime"
                                 ascending:YES];
}


-(NSArray *)getForecast{
    NSArray *entityItems = [self findAll];
    entityItems = [entityItems subarrayWithRange:NSMakeRange(0, 24)];
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    for(id entity in entityItems) {
        SolarActivityItem * soralItem = [[SolarActivityItem alloc] init];
        SolarEntity * entityItem = entity;
        soralItem.kIndex = entityItem.kIndex;
        soralItem.timeStartPeriod = entityItem.startTime;
        soralItem.timeMiddlePeriod = entityItem.middleTime;
        soralItem.timeEndPeriod = entityItem.endTime;
        soralItem.dayMonthPeriod = entityItem.dayMonth;
        soralItem.dateTimePeriod = entityItem.dateTime;
        soralItem.unixTimeStartPeriod = [entityItem.unixTime doubleValue];
        [tmpArray addObject:soralItem];
    }
    NSArray * solarItems = [tmpArray sortedArrayUsingDescriptors:@[
                                                              [NSSortDescriptor sortDescriptorWithKey:@"unixTimeStartPeriod" ascending:YES]]];
    return solarItems;
}


-(void)deleteSolarItem:(SolarEntity *)oldItem {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [oldItem MR_deleteEntityInContext:localContext];
    }];
}


@end
