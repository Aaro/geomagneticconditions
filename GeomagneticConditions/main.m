//
//  main.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 06.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
