//
//  SolarActivityItem.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 06.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SolarActivityItem : NSObject

@property (strong, nonatomic) NSString *timeStartPeriod;
@property (strong, nonatomic) NSString *timeEndPeriod;
@property (strong, nonatomic) NSString *timeMiddlePeriod;
@property (strong, nonatomic) NSString *dayMonthPeriod;
@property (strong, nonatomic) NSString *dateTimePeriod;
@property (strong, nonatomic) NSString *kIndex;
@property (assign, nonatomic) NSTimeInterval unixTimeStartPeriod;

@end
