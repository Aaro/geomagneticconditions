//
//  ForecastTableViewCell.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 09.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *kIndexLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *colorKIndexView;

@end
