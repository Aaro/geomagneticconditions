//
//  DataCache.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 09.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DataCache : NSObject

@property (nonatomic, copy) void (^dataChangeListener)(void);

+(DataCache *)sharedInstance;
-(void)saveDataInCache:(NSArray *)solarItems;
-(NSArray *)findAll;
-(NSArray *)getSolarItems;
-(NSArray *)getForecast;
-(void)setListener:(void (^)(void)) listener;


@end
