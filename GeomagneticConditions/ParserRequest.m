//
//  ParserRequest.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 09.01.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

#import "ParserRequest.h"
#import "SolarActivityItem.h"

@implementation ParserRequest


static ParserRequest *_sharedInstance = nil;
+(ParserRequest *)sharedInstance{
    @synchronized(self) {
        if (!_sharedInstance) {
            _sharedInstance = [[ParserRequest alloc] init];
        }
    }
    return _sharedInstance;
}


-(NSArray *)parseAnswer:(NSString *)answer {
    NSArray *solarItemsTmp = [answer componentsSeparatedByString:@"\n"];
    NSArray *daysArray = [self getDaysFromData:[solarItemsTmp[16] componentsSeparatedByString:@" "]];
    //17-24 indexes with information kindexes
    NSArray *dataAboutFirstDay = [self getSolarItemsForFirstDay:solarItemsTmp day:daysArray[0]];
    NSArray *dataAboutSecondtDay = [self getSolarItemsForSecondDay:solarItemsTmp day:daysArray[1]];
    NSArray *dataAboutThirdDay = [self getSolarItemsForThirdDay:solarItemsTmp day:daysArray[2]];
    NSArray *allSolarItems = [NSArray arrayWithObjects:dataAboutFirstDay, dataAboutSecondtDay, dataAboutThirdDay, nil];
    return allSolarItems;
}


-(NSArray *)getDaysFromData:(NSArray *)dataArray {
    NSMutableArray *mutableDataArray = [[NSMutableArray alloc] initWithArray:dataArray];
     [mutableDataArray removeObject:@""];
    //Today
    NSString *todayDateString = [NSString stringWithFormat:@"%@-%@", mutableDataArray[1], mutableDataArray[0]];
    
    //tomorrow
    NSString *tomorrowDateString = [NSString stringWithFormat:@"%@-%@", mutableDataArray[3], mutableDataArray[2]];
    
    //day after tomorrow
    NSString *afterTomorrowDateString = [NSString stringWithFormat:@"%@-%@", mutableDataArray[5], mutableDataArray[4]];
    
    NSArray *daysArray = [NSArray arrayWithObjects:todayDateString, tomorrowDateString, afterTomorrowDateString, nil];
    return daysArray;
}


-(NSArray *)getSolarItemsForFirstDay:(NSArray *)dataArray day:(NSString *)day {
    NSMutableArray *kpIndexesFirstDay = [[NSMutableArray alloc] init];
    NSInteger time = 0;
    NSInteger stepTime = 3;
    //use 17-24 indexes
    for (NSUInteger index = 17; index<25; index++){
        SolarActivityItem *item = [[SolarActivityItem alloc]init];
        NSArray *indexArray = [dataArray[index] componentsSeparatedByString:@" "];
        NSMutableArray *mutableIndexArray = [[NSMutableArray alloc] initWithArray:indexArray];
        [mutableIndexArray removeObject:@""];
        NSTimeInterval unixTime  = [[NSDate date] timeIntervalSince1970];
        
        item.kIndex = mutableIndexArray[1];
        item.timeStartPeriod = [NSString stringWithFormat:@"%ld:00", (long)time];
        item.timeMiddlePeriod = [NSString stringWithFormat:@"%ld:30", (long)time+1];
        item.timeEndPeriod = [NSString stringWithFormat:@"%ld:00", (long)time+3];
        item.dayMonthPeriod = [NSString stringWithFormat:@"%@", day];
        item.dateTimePeriod = [NSString stringWithFormat:@"%ld:00-%ld:00 \n%@", (long)time,(long)time+stepTime , day];
        
        item.unixTimeStartPeriod = unixTime;
        
        time = time+stepTime;
        [kpIndexesFirstDay addObject:item];
    }
    NSArray *kpIndexes = [NSArray arrayWithArray:kpIndexesFirstDay];
    return kpIndexes;
}



-(NSArray *)getSolarItemsForSecondDay:(NSArray *)dataArray day:(NSString *)day {
    NSMutableArray *kpIndexesSecondDay = [[NSMutableArray alloc] init];
    NSInteger time = 0;
    NSInteger stepTime = 3;
    //use 17-24 indexes
    for (NSUInteger index = 17; index<25; index++){
        SolarActivityItem *item = [[SolarActivityItem alloc]init];
        NSArray *indexArray = [dataArray[index] componentsSeparatedByString:@" "];
        NSMutableArray *mutableIndexArray = [[NSMutableArray alloc] initWithArray:indexArray];
        [mutableIndexArray removeObject:@""];
        NSTimeInterval unixTime  = [[NSDate date] timeIntervalSince1970];
        
        item.kIndex = mutableIndexArray[2];
        item.timeStartPeriod = [NSString stringWithFormat:@"%ld:00", (long)time];
        item.timeMiddlePeriod = [NSString stringWithFormat:@"%ld:30", (long)time+1];
        item.timeEndPeriod = [NSString stringWithFormat:@"%ld:00", (long)time+3];
        item.dayMonthPeriod = [NSString stringWithFormat:@"%@", day];
        item.dateTimePeriod = [NSString stringWithFormat:@"%ld:00-%ld:00 \n%@", (long)time,(long)time+stepTime , day];
        item.unixTimeStartPeriod = unixTime;
        
        time = time+stepTime;
        [kpIndexesSecondDay addObject:item];
    }
    NSArray *kpIndexes = [NSArray arrayWithArray:kpIndexesSecondDay];
    return kpIndexes;
}


-(NSArray *) getSolarItemsForThirdDay:(NSArray *)dataArray day:(NSString *)day {
    NSMutableArray *kpIndexesThirdDay = [[NSMutableArray alloc] init];
    NSInteger time = 0;
    NSInteger stepTime = 3;
    //use 17-24 indexes
    for (NSUInteger index = 17; index<25; index++){
        SolarActivityItem *item = [[SolarActivityItem alloc]init];
        NSArray *indexArray = [dataArray[index] componentsSeparatedByString:@" "];
        NSMutableArray *mutableIndexArray = [[NSMutableArray alloc] initWithArray:indexArray];
        [mutableIndexArray removeObject:@""];
        NSTimeInterval unixTime  = [[NSDate date] timeIntervalSince1970];
        
        item.kIndex = mutableIndexArray[3];
        item.timeStartPeriod = [NSString stringWithFormat:@"%ld:00", (long)time];
        item.timeMiddlePeriod = [NSString stringWithFormat:@"%ld:30", (long)time+1];
        item.timeEndPeriod = [NSString stringWithFormat:@"%ld:00", (long)time+3];
        item.dayMonthPeriod = [NSString stringWithFormat:@"%@", day];
        item.dateTimePeriod = [NSString stringWithFormat:@"%ld:00-%ld:00 \n%@", (long)time,(long)time+stepTime , day];
        item.unixTimeStartPeriod = unixTime;
        
        time = time+ stepTime;
        [kpIndexesThirdDay addObject:item];
    }
    NSArray *kpIndexes = [NSArray arrayWithArray:kpIndexesThirdDay];
    return kpIndexes;
}


@end
