//
//  RequestManager.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 06.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestManager : NSObject

typedef void(^OnSuccess)(NSString* params);
typedef void(^OnFailure)(NSError* error);

+(RequestManager *)sharedInstance;

-(void)sendGetRequestSuccess:(OnSuccess)success failure:(OnFailure)failure;

@end


@protocol RequestDelegate <NSObject>
-(void) requestUpdateData:(RequestManager *) request;
@end
