//
//  ForecastViewController.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 06.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import "ForecastViewController.h"
#import "ForecastTableViewCell.h"
#import "DataCache.h"
#import "SolarActivityItem.h"


@implementation ForecastViewController


NSInteger const heightForHeaderInSection = 30;
NSInteger const heightForRowAtIndexPath = 62;
NSInteger const countSection = 3;
NSInteger const countRowsInSection = 8;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.forecastTableView setPagingEnabled: NO];
    [self.forecastTableView setSeparatorStyle: NO];
    [self.forecastTableView setAllowsSelection:NO];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    spinner.tag = 12;
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    self.forecastTableView.delegate = self;
    self.forecastTableView.dataSource = self;
    self.isNewData = false;
    [[DataCache sharedInstance] setListener:^{
        [self reloadTableView];
        [spinner stopAnimating];
        [self.forecastTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    }];
}


-(void)reloadTableView {
    self.dataArray = [[DataCache sharedInstance] getForecast];
    self.isNewData = true;
    [self.forecastTableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *string = nil;
    if(section == 0){
        string = @"Today";
    } else if(section == 1){
        string = @"Tomorrow";
    }else if(section == 2){
        string = @"Day after tomorrow";
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 5, tableView.frame.size.width, 18)];
    [label setFont: [UIFont fontWithName:@"HelveticaNeue-Light" size:15]];
    [label setTextColor:[UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1.0]];
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:231.0/255.0 green:231.0/255.0 blue:231.0/255.0 alpha:1.0]];
    return view;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return heightForHeaderInSection;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.isNewData)
        return countSection;
    return 0;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isNewData)
        return countRowsInSection;
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.row + indexPath.section * countRowsInSection;
    NSLog(@"Bla-bla");
    NSLog(@"%ld", (long)index);
    SolarActivityItem *item = [[SolarActivityItem alloc] init];
    item = self.dataArray[index];
    static NSString *simpleTableIdentifier = @"ForecastCell";
    ForecastTableViewCell *cell = (ForecastTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ForecastTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.kIndexLabel.text = item.kIndex;
    cell.dateTimeLabel.text = item.dateTimePeriod;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightForRowAtIndexPath;
}

@end
