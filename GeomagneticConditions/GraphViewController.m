//
//  GraphViewController.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 26.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import "GraphViewController.h"
#import "DataCache.h"
#import "SolarActivityItem.h"
#import "Colors.h"


@interface MyCustomFormatter: NSObject < IChartAxisValueFormatter>

- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis;

@end


@implementation MyCustomFormatter

- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis {
    NSUInteger day = value / 8.0;
    NSString *dayString;
    if (day == 0) {
       dayString = @"Today";
    } else if (day == 1) {
        dayString = @"Tomorrow";
    } else {
        dayString = @"After";
    }
    
    NSUInteger hour = (int)value % (int)8.0 * 3.0;
    return [NSString stringWithFormat:@"%@ %lu:00", dayString, (unsigned long)hour];
}

@end


@interface GraphViewController ()

@end

@implementation GraphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getSolarItems];
    [self customizeChart];
    [self generateDataSets];
    self.title = @"Stacked Bar Chart";
}


-(void)generateDataSets {
    
    BarChartData *data = [[BarChartData alloc] init];
    NSMutableArray* datasets = [[NSMutableArray alloc] init];
    NSMutableArray *dataEntries = [[NSMutableArray alloc] init];
    NSMutableArray *colorsEntries = [self getColorsArray];

    for (int i = 0; i < self.dataArray.count; i++) {
        SolarActivityItem *item = self.dataArray[i];
        BarChartDataEntry *dataEntry =  [[BarChartDataEntry alloc] initWithX:i y:[item.kIndex doubleValue]];
        [dataEntries addObject:dataEntry];
    }
    BarChartDataSet *dataset = [[BarChartDataSet alloc] initWithValues:dataEntries label:@" "];
    dataset.drawValuesEnabled = false;
    dataset.highlightEnabled = false;
    dataset.colors  = colorsEntries;
    
    [datasets addObject:dataset];
    [data setDataSets:datasets];
    [self.barChart setData:data];
}


-(NSMutableArray *) getColorsArray {
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.dataArray.count; i++) {
        SolarActivityItem *item = self.dataArray[i];
        if([item.kIndex isEqualToString:@"1"]) {
            [colors addObject: greenColor];
            
        } else if([item.kIndex isEqualToString:@"2"]) {
            [colors addObject: yellowColor];
            
        } else if([item.kIndex isEqualToString:@"3"]) {
            [colors addObject: orangeColor];
            
        } else if([item.kIndex isEqualToString:@"4"]) {
            [colors addObject: lightRedColor];
            
        } else if([item.kIndex isEqualToString:@"5"]) {
            [colors addObject: darkRedColor];
            
        } else if([item.kIndex isEqualToString:@"6"]) {
            [colors addObject: vinousColor];
            
        } else if([item.kIndex isEqualToString:@"7"]) {
            [colors addObject: lightPurpleColor];
            
        } else if([item.kIndex isEqualToString:@"8"]) {
            [colors addObject: darkRedColor];
        }
    }
    return colors;
}


-(void)customizeChart{
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    ChartYAxis *leftAxis = _barChart.leftAxis;
    leftAxis.valueFormatter = [[ChartDefaultAxisValueFormatter alloc] initWithFormatter:leftAxisFormatter];
    leftAxis.axisMinimum = 0.0;
    leftAxis.axisMaximum = 8.0;
    _barChart.rightAxis.enabled = NO;
    [_barChart setScaleYEnabled:false];
    [_barChart setScaleMinima:3 scaleY:1];
    _barChart.pinchZoomEnabled = NO;
    _barChart.descriptionText = @"";
    [_barChart.xAxis setLabelPosition:XAxisLabelPositionBottom];
    [_barChart.legend setPosition:ChartLegendPositionAboveChartRight];
    _barChart.xAxis.labelRotationAngle = -30;
    _barChart.leftAxis.drawGridLinesEnabled = false;
    _barChart.xAxis.drawGridLinesEnabled = false;
    [_barChart.xAxis setValueFormatter:[[MyCustomFormatter alloc] init]];
}


-(void)getSolarItems{
    self.dataArray = [[DataCache sharedInstance] getForecast];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight
{
    NSLog(@"chartValueSelected, stack-index %ld", (long)highlight.stackIndex);
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}


#pragma mark - Graph


/*

- (void)setDataCount:(int)count range:(double)range
{
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < count; i++)
    {
        double mult = (range + 1);
        double val1 = (double) (arc4random_uniform(mult) + mult / 3);
        double val2 = (double) (arc4random_uniform(mult) + mult / 3);
        double val3 = (double) (arc4random_uniform(mult) + mult / 3);
        
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@(val1), @(val2), @(val3)]]];
    }
    
    BarChartDataSet *set1 = nil;
    if (_barChart.data.dataSetCount > 0)
    {
        set1 = (BarChartDataSet *)_barChart.data.dataSets[0];
        set1.values = yVals;
        [_barChart.data notifyDataChanged];
        [_barChart notifyDataSetChanged];
    }
    else
    {
        set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@"Statistics Vienna 2014"];
        set1.colors = @[ChartColorTemplates.material[0], ChartColorTemplates.material[1], ChartColorTemplates.material[2]];
        set1.stackLabels = @[@"Births", @"Divorces", @"Marriages"];
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.maximumFractionDigits = 1;
        formatter.negativeSuffix = @" $";
        formatter.positiveSuffix = @" $";
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:7.f]];
        [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:formatter]];
        [data setValueTextColor:UIColor.whiteColor];
        
        _barChart.fitBars = YES;
        _barChart.data = data;
    }
}


*/



/*
 
 -(void)generateDataSets2 {
 
 BarChartData *data = [[BarChartData alloc] init];
 NSMutableArray* datasets = [[NSMutableArray alloc] init];
 
 NSMutableArray *dataEntries1Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries2Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries3Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries4Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries5Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries6Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries7Index = [[NSMutableArray alloc] init];
 NSMutableArray *dataEntries8Index = [[NSMutableArray alloc] init];
 
 for (int i = 0; i < self.dataArray.count; i++) {
 SolarActivityItem *item = self.dataArray[i];
 BarChartDataEntry *dataEntry =  [[BarChartDataEntry alloc] initWithX:i y:[item.kIndex doubleValue]];
 if([item.kIndex isEqualToString:@"1"]) {
 [dataEntries1Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"2"]) {
 [dataEntries2Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"3"]) {
 [dataEntries3Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"4"]) {
 [dataEntries4Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"5"]) {
 [dataEntries5Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"6"]) {
 [dataEntries6Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"7"]) {
 [dataEntries7Index addObject:dataEntry];
 
 } else if([item.kIndex isEqualToString:@"8"]) {
 [dataEntries8Index addObject:dataEntry];
 }
 }
 
 BarChartDataSet *dataset1 = [[BarChartDataSet alloc] initWithValues:dataEntries1Index label:@"index = 1"];
 dataset1.drawValuesEnabled = false;
 dataset1.highlightEnabled = false;
 [dataset1 setColor:[UIColor colorWithRed:0.0/255.0 green:153.0/255.0 blue:76.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset1];
 
 
 BarChartDataSet *dataset2 = [[BarChartDataSet alloc] initWithValues:dataEntries2Index label:@"index = 2"];
 [dataset2 setColor:[UIColor colorWithRed:250.0/255.0 green:235.0/255.0 blue:28.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset2];
 
 
 BarChartDataSet *dataset3 = [[BarChartDataSet alloc] initWithValues:dataEntries3Index label:@"index = 3"];
 [dataset3 setColor:[UIColor colorWithRed:250.0/255.0 green:161.0/255.0 blue:28.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset3];
 
 
 BarChartDataSet *dataset4 = [[BarChartDataSet alloc] initWithValues:dataEntries4Index label:@"index = 4"];
 [dataset4 setColor:[UIColor colorWithRed:255.0/255.0 green:85.0/255.0 blue:0.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset4];
 
 
 BarChartDataSet *dataset5 = [[BarChartDataSet alloc] initWithValues:dataEntries5Index label:@"index = 5"];
 [dataset5 setColor:[UIColor colorWithRed:199.0/255.0 green:36.0/255.0 blue:30.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset5];
 
 
 BarChartDataSet *dataset6 = [[BarChartDataSet alloc] initWithValues:dataEntries6Index label:@"index = 6"];
 [dataset6 setColor:[UIColor colorWithRed:209.0/255.0 green:31.0/255.0 blue:97.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset6];
 
 
 BarChartDataSet *dataset7 = [[BarChartDataSet alloc] initWithValues:dataEntries7Index label:@"index = 7"];
 [dataset7 setColor:[UIColor colorWithRed:168.0/255.0 green:41.0/255.0 blue:126.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset7];
 
 
 BarChartDataSet *dataset8 = [[BarChartDataSet alloc] initWithValues:dataEntries8Index label:@"index = 8"];
 [dataset8 setColor:[UIColor colorWithRed:118.0/255.0 green:16.0/255.0 blue:115.0/255.0 alpha:1.0]
 ];
 [datasets addObject: dataset8];
 
 
 [data setDataSets:datasets];
 [self.barChart setData:data];
 }

 
 
 */


@end
