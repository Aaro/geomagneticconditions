//
//  RequestManager.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 06.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import "RequestManager.h"

@implementation RequestManager

NSString *const urlStr = @"http://services.swpc.noaa.gov/text/3-day-geomag-forecast.txt";


static RequestManager *_sharedInstance = nil;
+(RequestManager *)sharedInstance{
    @synchronized(self) {
        if (!_sharedInstance) {
            _sharedInstance = [[RequestManager alloc] init];
        }
    }
    return _sharedInstance;
}


- (NSMutableURLRequest *)getRequest {
    NSURL * url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:url];
    return request;
}


-(void)sendGetRequestSuccess:(OnSuccess)success failure:(OnFailure)failure{
    NSMutableURLRequest *request = [self getRequest];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                                if (error) {
                                                    NSLog(@"Error: sendGetRequestWithURL");
                                                    // Handle error...
                                                    return;
                                                }
                                                if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                                    NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                                    NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                                }
                                                NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                NSLog(@"Response Body:\n%@\n", body);
                                                if (response != nil) {
                                                    if (success != nil) {
                                                        success(body);
                                                    }
                                                } else if (failure != nil) {
                                                    failure(error);
                                                }
                                            }];
    [task resume];
}

@end
