//
//  HistoryTableViewController.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 15.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property(strong, nonatomic) NSArray *historyArray;
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;

@end
