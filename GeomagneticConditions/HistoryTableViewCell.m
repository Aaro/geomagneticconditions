//
//  HistoryTableViewCell.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 15.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import "HistoryTableViewCell.h"
#import "Colors.h"

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setColorForViewColor];

    // Configure the view for the selected state
}


-(void)setColorForViewColor {
    NSString *kIndex = self.kIndexLabel.text;
    UIColor *color = [[UIColor alloc] init];
    if([kIndex  isEqual: @"1"]){
        color = greenColor;
    }
    if([kIndex  isEqual: @"2"]){
        color = yellowColor;
    }
    if([kIndex  isEqual: @"3"]){
        color = orangeColor;
    }
    if([kIndex  isEqual: @"4"]){
        color = lightRedColor;
    }
    if([kIndex  isEqual: @"5"]){
        
        color = darkRedColor;
    }
    if([kIndex  isEqual: @"6"]){
        color = vinousColor;
    }
    if([kIndex  isEqual: @"7"]){
        color = lightPurpleColor;
    }
    if([kIndex  isEqual: @"8"]){
        color = darkPurpleColor;
    }
    self.viewColor.backgroundColor = color;
}


@end
