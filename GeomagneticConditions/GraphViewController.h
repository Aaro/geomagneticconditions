//
//  GraphViewController.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 26.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;



@interface GraphViewController : UIViewController

@property (strong, atomic) NSArray *dataArray;
@property (weak, nonatomic) IBOutlet BarChartView *barChart;

@end
