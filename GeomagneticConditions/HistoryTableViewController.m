//
//  HistoryTableViewController.m
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 15.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import "HistoryTableViewController.h"
#import "DataCache.h"
#import "SolarActivityItem.h"
#import "HistoryTableViewCell.h"
#import <Crashlytics/Crashlytics.h>


@interface HistoryTableViewController ()

@end

@implementation HistoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.historyTableView.delegate = self;
    self.historyTableView.dataSource = self;
    [self.historyTableView setAllowsSelection:NO];
    self.historyArray = [[DataCache sharedInstance] getSolarItems];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.historyArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SolarActivityItem *solarItem = self.historyArray[indexPath.row];
    static NSString *simpleTableIdentifier = @"historyCell";
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.kIndexLabel.text = solarItem.kIndex;
    cell.dateLabel.text = solarItem.dateTimePeriod;
    return cell;
}

@end
