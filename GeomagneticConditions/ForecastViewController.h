//
//  ForecastViewController.h
//  GeomagneticConditions
//
//  Created by Надежда Шальнова on 06.12.16.
//  Copyright © 2016 Nadezhda Shalnova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *forecastTableView;
@property (strong, nonatomic) NSArray *dataArray;
@property (assign, nonatomic) BOOL isNewData;
@end
